<?php

    require ('animal.php');
    require ('Frog.php');
    require ('Ape.php');


    $sheep = new Animal("shaun");
    $sungokong = new Ape("kera sakti");
    $kodok = new Frog("buduk");

    echo "Nama: ". $sheep->name ."<br>"; // "shaun"
    echo "Jumlah kaki: ". $sheep->legs ."<br>"; // 2
    echo "Darah dingin: ". $sheep->cold_blooded ."<br>"; // false

 

    echo "<br>";
    echo "Nama: ". $sungokong->name ."<br>";
    echo "Jumlah kaki: ". $sungokong->legs ."<br>";
    echo $sungokong->yell() ."<br>"; // "Auooo";
    //var_dump($sungokong);

    

    echo "<br>";
    echo "Nama: ". $kodok->name ."<br>";
    echo "Jumlah kaki: ". $kodok->legs ."<br>";
    echo $kodok->jump() ; // "hop hop";
    //var_dump($kodok);
 
    
?>
